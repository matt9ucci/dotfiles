. "$HOME/.dotfile"

# ignorespace and ignoredups
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

. /etc/profile.d/bash_completion.sh

[ -x "$HOME/.dotnet/dotnet" ] && export PATH="$HOME/.dotnet:$PATH"
[ -x "$(command -v aws_completer)" ] && complete -C aws_completer aws

if [ -x "$(command -v dotnet)" ]
then
	_dotnet_bash_completion() {
		local word=${COMP_WORDS[COMP_CWORD]}
		local completions="$(dotnet complete --position "$COMP_POINT" "$COMP_LINE" 2>/dev/null)"
		if [ $? -ne 0 ]
		then
			completions=""
		fi
		COMPREPLY=( $(compgen -W "$completions" -- "$word") )
	}
	complete -f -F _dotnet_bash_completion dotnet
fi

[ -x "$(command -v npm)" ] && eval "$(npm completion)"

[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"
