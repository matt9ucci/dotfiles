# dotfiles

## Install

```sh
git clone https://matt9ucci@bitbucket.org/matt9ucci/dotfiles.git $HOME/dotfiles
chmod +x $HOME/dotfiles/install.sh
sh $HOME/dotfiles/install.sh
```

## Links

### POSIX - The Open Group Base Specifications Issue 7, 2018 edition

* [Environment Variables](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html)
* [Shell Command Language](https://pubs.opengroup.org/onlinepubs/9699919799/idx/shell.html)
	- [Command Substitution](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_03)
* [Utilities](https://pubs.opengroup.org/onlinepubs/9699919799/idx/utilities.html)
	- [cd](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/cd.html)
	- [dirname](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/dirname.html)
	- [ls](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/ls.html)
	- [mkdir](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/mkdir.html)
	- [pwd](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/pwd.html)
	- [sh](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/sh.html)
