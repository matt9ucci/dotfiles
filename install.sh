set -eu

dir_name="$(cd "$(dirname "$0")" && pwd)"

ln -fs "$dir_name/.dotfile" "$HOME/.dotfile"
ln -fs "$dir_name/.inputrc" "$HOME/.inputrc"
ln -fs "$dir_name/bash/.bashrc" "$HOME/.bashrc"

if [ $(command -v pwsh) ]
then
	pwsh -c 'git clone https://github.com/matt9ucci/PSProfiles.git (Split-Path $PROFILE)'
fi
